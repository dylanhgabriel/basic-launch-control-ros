#include "ros/ros.h"
#include "sensor_msgs/Imu.h"
#include "/home/dyl/Project/Subscriber/ros_ws/src/controller/include/controller/cont.h"

// Run sub and pub in "cont.cpp" implementation file
int main(int argc, char** argv)  { 
    ros::init(argc, argv, "controller");  
    accelCont accelCont;
    ros::spin();  
    return 0;
}