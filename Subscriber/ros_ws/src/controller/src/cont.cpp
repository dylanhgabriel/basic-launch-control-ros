#include<iostream>
#include<vector>
#include "/home/dyl/Project/Subscriber/ros_ws/src/controller/include/controller/cont.h"

// Round eqn (Outside function from internet)
int accelCont::roundUp(int numToRound, int multiple){
    if (multiple == 0)
        return numToRound;

    int remainder = numToRound % multiple;
    if (remainder == 0)
        return numToRound;

    return numToRound + multiple - remainder;
}

// Vehicle Parameters
void accelCont::paramRead(){
    Rwheel = 0.226;                             // Wheel effective radius (m)
    rFinal = 3.818;                             // Final drive ratio
    rGear = {2.750, 2.000, 1.444, 1.150};       // Transmission gear ratios (4 speed)
    rPrimary = 2.073;                           // Engine primary reduction
    slipRat = {0.06, 0.08, 0.10, 0.12};         // Slip ratio targets based on potentiometer position (pot)

    // How much engine power should be reduced based on error in RPM (100 RPM incremments)
    reductVector = {0, 0.25 ,0.25 ,0.25 ,0.5 ,0.5 ,0.75 ,0.75 ,1 ,1 ,1};     // 1 = full igniton cut, less than one is % timing retard
}
// Error Calc
double accelCont::RPMcalc(){
    paramRead();
    // Calculate the target engine RPM based on current ground speed, slip ratio target, and vehicle parameters
    RPMtarget = ((groundSpeed/Rwheel)*(60/(2*3.14159))*rFinal*rGear[gear - 1]*rPrimary)*(1+slipRat[pot]);
    return RPMtarget;
}

// Controller 
double accelCont::controller(){
    // Calculate error
    RPMtarget = RPMcalc();
    errRPM = RPM - RPMtarget;
    // Round error so it can match resolution of "reductVector"
    errRPMtable = roundUp(errRPM,100);
    if (errRPMtable < 0) errRPMtable = 0;
    if (errRPMtable > 1000) errRPMtable = 1000;
    // Find index
    loc = (errRPMtable)/(100);
    // determine engine power reduction based on error and "reductVector"
    reduct = 0;
    if (groundSpeed > 0 && thrPos > 95) reduct = reductVector[loc];
    return reduct;
}
// Initialize subscriber and republisher
accelCont::accelCont() {  
    sub = n.subscribe("data_pub/data",1,&accelCont::mCall, this);  
    pub = n.advertise<sensor_msgs::Imu>("controller_repub",50);
}

// Substribe to msgs
void accelCont::mCall (const sensor_msgs::Imu::ConstPtr& msg) {
    ROS_INFO("Time: %u", msg->header.stamp.sec);  
    ROS_INFO("Seq %u", msg->header.seq); 

    ROS_INFO("gyrX: %lf",msg->angular_velocity.x);   
    ROS_INFO("gyrY: %lf",msg->angular_velocity.y);  
    ROS_INFO("gyrZ: %lf",msg->angular_velocity.z);  

    ROS_INFO("accX: %lf",msg->linear_acceleration.x);
    ROS_INFO("accY: %lf",msg->linear_acceleration.y);  
    ROS_INFO("accZ: %lf",msg->linear_acceleration.z);  

    groundSpeed  = msg->angular_velocity.x; 
    driveSpeed  = msg->angular_velocity.y;  
    RPM  = msg->angular_velocity.z; 

    thrPos  = msg->linear_acceleration.x; 
    gear  = msg->linear_acceleration.y; 
    pot  = msg->linear_acceleration.z;
    // Republish the data
    publishData(); 
}

// Publisher
void accelCont::publishData() {  
    sensor_msgs::Imu imu_msg;    
    imu_msg.header.stamp = ros::Time::now();    
    imu_msg.header.frame_id = "frame_id1";    
    // do math using subscribed values before republishing
    reduct = controller();
    imu_msg.angular_velocity.x = groundSpeed;
    imu_msg.angular_velocity.y = driveSpeed;
    imu_msg.angular_velocity.z = RPM;  

    imu_msg.linear_acceleration.x = thrPos;    
    imu_msg.linear_acceleration.y = gear;    
    imu_msg.linear_acceleration.z = pot;
    // Publish new engine reduction parameter to be read by engine ECU
    imu_msg.orientation.w = reduct;
       
    pub.publish(imu_msg);  
    
}
