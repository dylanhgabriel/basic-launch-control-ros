#include<iostream>
#include<vector>
#include "ros/ros.h"
#include "sensor_msgs/Imu.h"

#ifndef CONT_H
#define CONT_H

using namespace std;
class accelCont {  
    public:    
        accelCont(); 
        double time;
        double driveSpeed;
        double groundSpeed;
        double RPM;
        double thrPos;
        int gear;
        int pot;
        double Rwheel;
        double rFinal;
        vector <double> rGear;
        double rPrimary;
        double RPMtarget;
        double errRPM;
        double RPMcalc();
        double controller();
        int roundUp(int numToRound, int multiple); 
        int errRPMtable;
        double reduct;
        vector <double> reductVector;
        int loc;
        vector <double> slipRat;
        void paramRead();
    private:    
        ros::NodeHandle n;    
        ros::Subscriber sub;     
        ros::Publisher pub;     
        void mCall(const sensor_msgs::Imu::ConstPtr& msg);
        void publishData();     
    };


#endif