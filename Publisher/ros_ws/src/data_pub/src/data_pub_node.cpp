#include "ros/ros.h"
#include "std_msgs/String.h"
#include "sensor_msgs/Imu.h"
#include<fstream>

using namespace std;

int main(int argc, char** argv){  
    // Initialize Publisher
    ros::init(argc,argv,"data_pub");
    ros::NodeHandle pnh("~"); 
    std::string frame_id1;  
    pnh.param<std::string>("frame_id",frame_id1,"imu_frame");  
    ros::Publisher imu_pub;    
    imu_pub = pnh.advertise<sensor_msgs::Imu>("data",50);
    ros::Rate loop_rate(50);
    // Load data to publish
    fstream data;
    data.open("/home/dyl/Project/Publisher/ros_ws/src/data_pub/accelData.txt");
    double time;
    double driveSpeed;
    double groundSpeed;
    double RPM;
    double thrPos;
    double gear;
    double pot;
    // Load data to each msg at each timestep
    while(ros::ok() && !data.eof()) { 
        data >> time >> driveSpeed >> groundSpeed >> RPM >> thrPos >> gear >> pot;

        // data message
        sensor_msgs::Imu imu_msg;    
        imu_msg.header.stamp = ros::Time::now();    
        imu_msg.header.frame_id = frame_id1;  

        imu_msg.angular_velocity.x = groundSpeed;
        imu_msg.angular_velocity.y = driveSpeed;
        imu_msg.angular_velocity.z = RPM;  

        imu_msg.linear_acceleration.x = thrPos;    
        imu_msg.linear_acceleration.y = gear;    
        imu_msg.linear_acceleration.z = pot;

        imu_pub.publish(imu_msg);   

        ros::spinOnce();    
        loop_rate.sleep();     
    }
    data.close();
}